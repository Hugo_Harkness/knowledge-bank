cd ../DLLs

tar -f DLLs.tar -c platforms
tar -f DLLs.tar -r styles
tar -f DLLs.tar -r *.dll

move DLLs.tar ..\build-KnowledgeBank-Desktop_Qt_5_10_0_MinGW_32bit-Release\release\DLLs.tar
cd ..\build-KnowledgeBank-Desktop_Qt_5_10_0_MinGW_32bit-Release\release

tar -f DLLs.tar -r *.exe

move DLLs.tar ..\..\inst\Knowledge_Bank_1.3.tar
