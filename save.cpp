/*  Knowledge Bank
 *  Copyright (C) 2018  Hugo Tritz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "save.h"

#include <QFile>
#include <QTextStream>
#include <iostream>
#include <sstream>
#include <qstandardpaths.h>

 QString stdpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/KnowledgeBank/");
 QString filepath(stdpath + "save");

void load(QMap<QString, QString> &save, int &nbEntry){

    QFile file(filepath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        std::cout << "file not found!" << std::endl;
         return;
    }
    QTextStream in(&file);

    QString line;
    QString key;
    QString definition;

    nbEntry = std::stoi(STR(in.readLine()));

    line = in.readLine();
    while(line != "#end"){

        if(line.startsWith("#Entry")){
            line = in.readLine();
            while(line != "#endEnt"){

                if (line.startsWith("§Key")) {
                    line = in.readLine();
                    key = line;
                }else if (line.startsWith("§Definition")) {
                    line = in.readLine();
                    definition = "";
                    QTextStream ss(&definition);
                    while(line != "§endDef"){
                        ss << line << "\n";
                        line = in.readLine();
                    }
                }

                line = in.readLine();
            }
            save.insert(key, definition);

        }

        line = in.readLine();

    }

    file.close();
}

void write(const QMap<QString, QString>& save, int nbEntry){

    QFile file(filepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << nbEntry << "\n";

    QString p1 = "#Entry\n§Key\n";
    QString p2 = "\n§Definition\n";
    QString p3 = "§endDef\n#endEnt\n\n";

    QMapIterator<QString, QString> i(save);
    while (i.hasNext()) {
        i.next();
        out << p1 << i.key() << p2 << i.value() << p3;
    }
    out << "#end";

    file.close();
}

void loadSaves(std::vector<QString>& saves){

    QFile file(stdpath+ "save");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        std::cout << "file not found!" << std::endl;
         return;
    }
    QTextStream in(&file);

    QString line;

    line = in.readLine();
    while(!line.startsWith("#end")){

        if(line.startsWith("#Save")){
            line = in.readLine();
            saves.push_back(line);
        }
        line = in.readLine();
    }
    file.close();
}

void writeSaves(const std::vector<QString>& saves){
    QFile file(stdpath+"save");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);

    foreach(QString saveN, saves){
        out << "#Save\n" << saveN << "\n";
    }

    out << "#end";

    file.close();
}
