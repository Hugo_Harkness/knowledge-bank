/*  Knowledge Bank
 *  Copyright (C) 2018  Hugo Tritz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "save.h"
#include "filechooser.h"
#include <iostream>
#include <QDir>

QMap<QString, QString> saveNull;
QMap<QString, QString> save;
int nbEntry;
std::vector<QString> saves;
FileChooser* filleChooser;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    filleChooser = new FileChooser();
    if(!QDir(stdpath).exists()){
        QDir().mkdir(stdpath);
        std::cout << "created " << STR(stdpath) << std::endl;
    }
    ui->setupUi(this);
    this->setFixedSize(QSize(419, 301));
    loadSaves(saves);
    loadListSave();
    ui->widgetImport->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadListSave(){
    ui->listsave->clear();
    foreach(QString saveN, saves){
        ui->listsave->addItem(saveN);
    }
    ui->listsave->setCurrentRow(0);
}

void MainWindow::loadList(){
    ui->List->clear();
    QMapIterator<QString, QString> i(save);
    while (i.hasNext()) {
        i.next();
        ui->List->addItem(i.key());
    }
    ui->List->setCurrentRow(0);
}

void MainWindow::on_shearchBar_textChanged(const QString &arg1)
{
    /*QMapIterator<QString, QString> i(save);
    unsigned int j =0;
    while (i.hasNext()) {
        i.next();
        std::string test = STR(i.key());
        if(test.find(STR((arg1))) == 0){
            ui->List->setCurrentRow(j);
            int dep = j+13;
            if(dep>nbEntry-1) dep = nbEntry-1;
            ui->List->scrollToItem(ui->List->item(dep));
            return;
        }
        j++;
    }*/

    ui->List->clear();

    QMapIterator<QString, QString> i(save);
        while (i.hasNext()) {
            i.next();
            std::string test = STR(i.key());
            if(test.find(STR((arg1))) != std::string::npos){
                ui->List->addItem(i.key());
            }
        }
      ui->List->setCurrentRow(0);
}

void MainWindow::on_List_itemSelectionChanged()
{
    QString key = ui->List->currentItem()->text();
    QString text ="\n" + save[key];
    ui->textBrowser->setText(text);
    ui->label->setText("<h2><u>" + key + "</u></h2>");
    ui->keyEdit->setText(key);
    ui->textEdit->setText(save[key]);
}

void MainWindow::on_delete_2_clicked()
{
    save.remove(ui->List->currentItem()->text());
    nbEntry--;
    write(save, nbEntry);
    QList<QListWidgetItem*> items = ui->List->selectedItems();
    foreach(QListWidgetItem* item, items)
    {
        ui->List->removeItemWidget(item);
        delete item;
    }
}

void MainWindow::on_add_clicked()
{
    save.insert(ui->keyEdit->text(), ui->textEdit->toPlainText()+"\n");
    loadList();
    nbEntry++;
    write(save, nbEntry);
}

void MainWindow::on_modify_clicked()
{
    save.remove(ui->List->currentItem()->text());
    save.insert(ui->keyEdit->text(), ui->textEdit->toPlainText());
    loadList();
    write(save, nbEntry);
}

void MainWindow::on_open_clicked()
{
    filepath = stdpath + ui->listsave->currentItem()->text();
    load(save, nbEntry);
    loadList();
    ui->widget->setVisible(false);
}

void MainWindow::on_newSave_clicked()
{
    std::string name = STR(ui->lineSave->text());
    foreach(QString saveN, saves){
        if(name == STR(saveN))
            return;
    }

    if (name != "save" && name != "") {
         filepath = stdpath + ui->lineSave->text();
         saves.push_back(ui->lineSave->text());
         writeSaves(saves);
         nbEntry = 0;
         ui->widget->setVisible(false);
         write(save, nbEntry);
    }

   loadListSave();
}

void MainWindow::on_delete_3_clicked()
{
    std::vector<QString> savesTmp;
    for(unsigned int i =0; i < saves.size(); i++){
        if(ui->listsave->currentItem()->text() != saves[i])
            savesTmp.push_back(saves[i]);
    }
    saves = savesTmp;

    writeSaves(saves);

    QString file = stdpath + ui->listsave->currentItem()->text();
    remove(STR(file));

   loadListSave();
}

void MainWindow::on_listsave_itemDoubleClicked(QListWidgetItem *item)
{
    filepath = stdpath + item->text();
    load(save, nbEntry);
    loadList();
    ui->widget->setVisible(false);
}

void MainWindow::on_Import_clicked()
{
    QString impPath = filleChooser->getImportPath();
    if( !impPath.isNull() )
    {
        filepath = impPath;
        load(save, nbEntry);
        QString name = impPath.split("/").last();
        name = name.left(name.lastIndexOf("."));
        ui->widget->setEnabled(false);
        ui->widgetImport->setVisible(true);
        ui->importNameline->setText(name);
    }
}

void MainWindow::on_Export_clicked()
{
     filepath = stdpath + ui->listsave->currentItem()->text();
     load(save, nbEntry);
     QString expPath = filleChooser->getExportPath(ui->listsave->currentItem()->text());
     if( !expPath.isNull() )
     {
        filepath = expPath;
        write(save, nbEntry);
     }
}

void MainWindow::on_importName_clicked()
{
    std::string name = STR(ui->importNameline->text());
    foreach(QString saveN, saves){
        if(name == STR(saveN))
            return;
    }

    if (name != "save" && name != "") {
         filepath = stdpath + ui->importNameline->text();
         saves.push_back(ui->importNameline->text());
         writeSaves(saves);
         write(save, nbEntry);
    }
    ui->widget->setEnabled(true);
    ui->widgetImport->setVisible(false);
    loadListSave();

}

void MainWindow::on_menu_clicked()
{
    nbEntry = 0;
    save = saveNull;
    ui->widget->setVisible(true);
}
