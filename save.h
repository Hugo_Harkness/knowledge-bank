/*  Knowledge Bank
 *  Copyright (C) 2018  Hugo Tritz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SAVE_H
#define SAVE_H

#define STR(x) x.toLocal8Bit().constData()

#include <vector>
#include <QApplication>
#include <QMap>

extern QString stdpath;
extern QString filepath;

void load(QMap<QString, QString>& save, int& nbEntry);
void write(const QMap<QString, QString>& save, int nbEntry);


void loadSaves(std::vector<QString>& saves);
void writeSaves(const std::vector<QString>& saves);

#endif // SAVE_H
