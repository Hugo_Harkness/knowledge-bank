/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[18];
    char stringdata0[306];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 25), // "on_shearchBar_textChanged"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 4), // "arg1"
QT_MOC_LITERAL(4, 43, 28), // "on_List_itemSelectionChanged"
QT_MOC_LITERAL(5, 72, 19), // "on_delete_2_clicked"
QT_MOC_LITERAL(6, 92, 14), // "on_add_clicked"
QT_MOC_LITERAL(7, 107, 17), // "on_modify_clicked"
QT_MOC_LITERAL(8, 125, 15), // "on_open_clicked"
QT_MOC_LITERAL(9, 141, 18), // "on_newSave_clicked"
QT_MOC_LITERAL(10, 160, 19), // "on_delete_3_clicked"
QT_MOC_LITERAL(11, 180, 29), // "on_listsave_itemDoubleClicked"
QT_MOC_LITERAL(12, 210, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(13, 227, 4), // "item"
QT_MOC_LITERAL(14, 232, 17), // "on_Import_clicked"
QT_MOC_LITERAL(15, 250, 17), // "on_Export_clicked"
QT_MOC_LITERAL(16, 268, 21), // "on_importName_clicked"
QT_MOC_LITERAL(17, 290, 15) // "on_menu_clicked"

    },
    "MainWindow\0on_shearchBar_textChanged\0"
    "\0arg1\0on_List_itemSelectionChanged\0"
    "on_delete_2_clicked\0on_add_clicked\0"
    "on_modify_clicked\0on_open_clicked\0"
    "on_newSave_clicked\0on_delete_3_clicked\0"
    "on_listsave_itemDoubleClicked\0"
    "QListWidgetItem*\0item\0on_Import_clicked\0"
    "on_Export_clicked\0on_importName_clicked\0"
    "on_menu_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x08 /* Private */,
       4,    0,   82,    2, 0x08 /* Private */,
       5,    0,   83,    2, 0x08 /* Private */,
       6,    0,   84,    2, 0x08 /* Private */,
       7,    0,   85,    2, 0x08 /* Private */,
       8,    0,   86,    2, 0x08 /* Private */,
       9,    0,   87,    2, 0x08 /* Private */,
      10,    0,   88,    2, 0x08 /* Private */,
      11,    1,   89,    2, 0x08 /* Private */,
      14,    0,   92,    2, 0x08 /* Private */,
      15,    0,   93,    2, 0x08 /* Private */,
      16,    0,   94,    2, 0x08 /* Private */,
      17,    0,   95,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_shearchBar_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_List_itemSelectionChanged(); break;
        case 2: _t->on_delete_2_clicked(); break;
        case 3: _t->on_add_clicked(); break;
        case 4: _t->on_modify_clicked(); break;
        case 5: _t->on_open_clicked(); break;
        case 6: _t->on_newSave_clicked(); break;
        case 7: _t->on_delete_3_clicked(); break;
        case 8: _t->on_listsave_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 9: _t->on_Import_clicked(); break;
        case 10: _t->on_Export_clicked(); break;
        case 11: _t->on_importName_clicked(); break;
        case 12: _t->on_menu_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
