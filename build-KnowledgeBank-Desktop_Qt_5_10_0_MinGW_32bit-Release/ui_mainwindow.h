/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLineEdit *shearchBar;
    QListWidget *List;
    QTabWidget *tabWidget;
    QWidget *tab;
    QTextEdit *textBrowser;
    QLabel *label;
    QWidget *tab_2;
    QPushButton *modify;
    QPushButton *add;
    QPushButton *delete_2;
    QLineEdit *keyEdit;
    QTextEdit *textEdit;
    QWidget *widget;
    QListWidget *listsave;
    QPushButton *delete_3;
    QPushButton *open;
    QPushButton *newSave;
    QTextBrowser *intro;
    QLineEdit *lineSave;
    QPushButton *Import;
    QPushButton *Export;
    QWidget *widgetImport;
    QFrame *frame;
    QLabel *label_2;
    QPushButton *importName;
    QLineEdit *importNameline;
    QPushButton *menu;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(422, 299);
        MainWindow->setMouseTracking(false);
        QIcon icon;
        icon.addFile(QStringLiteral(":/KB.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setTabShape(QTabWidget::Rounded);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        shearchBar = new QLineEdit(centralWidget);
        shearchBar->setObjectName(QStringLiteral("shearchBar"));
        shearchBar->setGeometry(QRect(10, 10, 171, 21));
        List = new QListWidget(centralWidget);
        List->setObjectName(QStringLiteral("List"));
        List->setGeometry(QRect(10, 40, 171, 251));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(190, 10, 221, 281));
        tabWidget->setTabPosition(QTabWidget::South);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        textBrowser = new QTextEdit(tab);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(0, 20, 231, 241));
        textBrowser->setFrameShape(QFrame::NoFrame);
        textBrowser->setReadOnly(true);
        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 0, 201, 21));
        label->setTextFormat(Qt::RichText);
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        modify = new QPushButton(tab_2);
        modify->setObjectName(QStringLiteral("modify"));
        modify->setGeometry(QRect(10, 230, 60, 21));
        add = new QPushButton(tab_2);
        add->setObjectName(QStringLiteral("add"));
        add->setGeometry(QRect(80, 230, 60, 21));
        delete_2 = new QPushButton(tab_2);
        delete_2->setObjectName(QStringLiteral("delete_2"));
        delete_2->setGeometry(QRect(150, 230, 60, 21));
        keyEdit = new QLineEdit(tab_2);
        keyEdit->setObjectName(QStringLiteral("keyEdit"));
        keyEdit->setGeometry(QRect(10, 10, 200, 21));
        textEdit = new QTextEdit(tab_2);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(10, 40, 200, 181));
        textEdit->setAcceptRichText(true);
        tabWidget->addTab(tab_2, QString());
        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setEnabled(true);
        widget->setGeometry(QRect(0, 0, 421, 301));
        widget->setAutoFillBackground(true);
        listsave = new QListWidget(widget);
        listsave->setObjectName(QStringLiteral("listsave"));
        listsave->setGeometry(QRect(10, 30, 191, 241));
        delete_3 = new QPushButton(widget);
        delete_3->setObjectName(QStringLiteral("delete_3"));
        delete_3->setGeometry(QRect(350, 250, 61, 21));
        open = new QPushButton(widget);
        open->setObjectName(QStringLiteral("open"));
        open->setGeometry(QRect(210, 250, 61, 21));
        newSave = new QPushButton(widget);
        newSave->setObjectName(QStringLiteral("newSave"));
        newSave->setGeometry(QRect(210, 220, 61, 21));
        intro = new QTextBrowser(widget);
        intro->setObjectName(QStringLiteral("intro"));
        intro->setGeometry(QRect(210, 30, 201, 151));
        lineSave = new QLineEdit(widget);
        lineSave->setObjectName(QStringLiteral("lineSave"));
        lineSave->setGeometry(QRect(280, 220, 131, 21));
        Import = new QPushButton(widget);
        Import->setObjectName(QStringLiteral("Import"));
        Import->setGeometry(QRect(210, 190, 91, 21));
        Export = new QPushButton(widget);
        Export->setObjectName(QStringLiteral("Export"));
        Export->setGeometry(QRect(319, 190, 91, 21));
        widgetImport = new QWidget(centralWidget);
        widgetImport->setObjectName(QStringLiteral("widgetImport"));
        widgetImport->setEnabled(true);
        widgetImport->setGeometry(QRect(100, 120, 241, 71));
        widgetImport->setAutoFillBackground(false);
        frame = new QFrame(widgetImport);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(0, 0, 241, 71));
        frame->setAutoFillBackground(true);
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(2);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 10, 121, 21));
        label_2->setTextFormat(Qt::RichText);
        importName = new QPushButton(frame);
        importName->setObjectName(QStringLiteral("importName"));
        importName->setGeometry(QRect(150, 40, 71, 21));
        importNameline = new QLineEdit(frame);
        importNameline->setObjectName(QStringLiteral("importNameline"));
        importNameline->setGeometry(QRect(20, 40, 113, 21));
        menu = new QPushButton(centralWidget);
        menu->setObjectName(QStringLiteral("menu"));
        menu->setGeometry(QRect(330, 270, 80, 21));
        MainWindow->setCentralWidget(centralWidget);
        tabWidget->raise();
        menu->raise();
        shearchBar->raise();
        List->raise();
        widget->raise();
        widgetImport->raise();

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Knowledge Bank", nullptr));
        shearchBar->setText(QString());
        label->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "view", nullptr));
        modify->setText(QApplication::translate("MainWindow", "Modify", nullptr));
        add->setText(QApplication::translate("MainWindow", "Add", nullptr));
        delete_2->setText(QApplication::translate("MainWindow", "Delete", nullptr));
        textEdit->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "edit", nullptr));
        delete_3->setText(QApplication::translate("MainWindow", "Delete", nullptr));
        open->setText(QApplication::translate("MainWindow", "Open", nullptr));
        newSave->setText(QApplication::translate("MainWindow", "New", nullptr));
        intro->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">Knowledge Bank</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Copyright (C) 2018  Hugo Tritz</p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; te"
                        "xt-indent:0px;\">    This program is free software: you can redistribute it and/or modify  it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</p></body></html>", nullptr));
        Import->setText(QApplication::translate("MainWindow", "Import", nullptr));
        Export->setText(QApplication::translate("MainWindow", "Export", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "<h2>List Name:", nullptr));
        importName->setText(QApplication::translate("MainWindow", "OK", nullptr));
        menu->setText(QApplication::translate("MainWindow", "<- Menu", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
