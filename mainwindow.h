/*  Knowledge Bank
 *  Copyright (C) 2018  Hugo Tritz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_shearchBar_textChanged(const QString &arg1);

    void on_List_itemSelectionChanged();

    void on_delete_2_clicked();

    void on_add_clicked();

    void on_modify_clicked();

    void on_open_clicked();

    void on_newSave_clicked();

    void on_delete_3_clicked();

    void on_listsave_itemDoubleClicked(QListWidgetItem *item);

    void on_Import_clicked();

    void on_Export_clicked();

    void on_importName_clicked();

    void on_menu_clicked();

private:
    Ui::MainWindow *ui;
    void loadListSave();
    void loadList();
};

#endif // MAINWINDOW_H
